#!/usr/bin/python

import subprocess
import shlex
from optparse import OptionParser
import gzip
import re
import os

file_path = os.path.dirname(os.path.abspath(__file__))
sentistrength_path = file_path + "/SentiStrength.jar"
sentistrength_data_path = file_path + "/data/"
#if options.filename == None:
#    raise Exception("Arquivo de entrada nescessario no parametro -f")


#Alec Larsen - University of the Witwatersrand, South Africa, 2012 import shlex, subprocess
def RateSentiment(sentiString):
    if sentiString == "" or sentiString == None:
        return "0 0 0"
    #open a subprocess using shlex to get the command line string into the correct args list format
    p = subprocess.Popen(shlex.split("java -jar " + sentistrength_path +" stdin sentidata " + sentistrength_data_path + " trinary"),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #communicate via stdin the string to be rated. Note that all spaces are replaced with +
    stdout_text, stderr_text = p.communicate(sentiString.replace(" ","+"))
    #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
    stdout_text = stdout_text.rstrip().replace("\t","")
    return stdout_text

def checkText(string_to_analyse):
    metodo_score = RateSentiment(string_to_analyse).split(" ")
    return int(metodo_score[2])
    
    # TODO: Remove this Matheus fix
    # pos_votes = abs(float(metodo_score[0]))
    # neg_votes = abs(float(metodo_score[1]))
    # total = pos_votes + neg_votes

    # #[-1,1,0] is neutral
    # if pos_votes == neg_votes:
    #     polaridade = 0
    # elif pos_votes + 1 == neg_votes:
    #     polaridade = 0
    # elif pos_votes == neg_votes + 1:
    #     polaridade = 0

    # elif pos_votes > neg_votes + 1:
    #     polaridade = 1*pos_votes/float(total)
    # elif pos_votes + 1 < neg_votes:
    #     polaridade = -1*neg_votes/float(total)

    return polaridade
    
if __name__ == "__main__":
   
    parser = OptionParser()
    parser.add_option('-f', '--file', dest='file')
    parser.add_option('-t', '--text', dest='text')
    parser.add_option('-p', '--just_polarity',action="store_true", dest='just_polarity')

    options, args = parser.parse_args()


    if options.text:
        print checkText(options.text)
    
    elif options.file:
        with open(options.file,"rb") as infile:
            for line in infile:
                print checkText(line)
    

#	with gzip.open(options.filename,"rb") as F:
#	    for tweet in F:
#		start_content = tweet.find("<t>")
#		end_content = tweet.find("</t>")
#		texto = tweet[start_content + 3 : end_content]
		# Remove \r from texto
#		if "\r" in texto:
#		    texto = texto.replace("\r","")

#		if options.just_polarity:
#		    print str(polaridade)
#		else:
#		    try:
#			id_tweet = tweet.split(" ")[1]
#		    except:
#			id_tweet = str(-1)
#		    print id_tweet + "\t" + texto + "\t" + str(polaridade) + "\t" + str(metodo_score)


