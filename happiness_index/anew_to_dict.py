import csv
import os
path_dir = os.path.dirname(os.path.abspath(__file__))
anew_file = os.path.join(path_dir,"anew.csv")

ANEW_set = {}

class anew_word():
    def __init__(self,word,wd_num,val_mn,val_sd,aro_mn,aro_sd,dom_mn,dom_sd,freq):
        self.word   = word
        self.wd_num = int(wd_num)
        self.val_mn = float(val_mn)
        self.val_sd = float(val_sd)
        self.aro_mn = float(aro_mn)
        self.aro_sd = float(aro_sd)
        self.dom_mn = float(dom_mn)
        self.dom_sd = float(dom_sd)
        try:
            self.freq   = int(freq)
        except:
            if freq == ".":
                self.freq = 0
            else:
                raise Exception("Houve um erro")

with open(anew_file,"rb") as csvfile:
    reader = csv.reader(csvfile,delimiter=",")
    for row in reader:
        word = row[0]
        ANEW_set[word] = anew_word(word,row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8])
