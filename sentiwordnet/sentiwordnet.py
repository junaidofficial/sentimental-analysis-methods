#!/home/matheus/vvv-venv/bin/python
import sys
import nltk
import os
import random

NEG = 0
POS = 0
NON = 0

class SynSet():
	def __init__(self,pos,ID,posScore,negScore,terms, gloss):
		self.pos = pos
		self.id = ID
		self.posScore = posScore
		self.negScore = negScore
		self.terms = self.getRawTerms(terms)
		self.gloss = gloss

		#Get Neutral
		# obj in context, is not object, is 1 - (pos + neg) read reference.
		self.obj = 1 - (self.pos + self.neg)

		#Add to all_terms
		key = self.terms[0]["term"]
		try:
			items = all_terms[key]
			items.append({"id":self.id,"terms":self.getTerms()})
		except:
			all_terms[key] = [{"id":self.id,"terms":self.getTerms()}]

	def getRawTerms(self,terms):
		list_terms = []
		terms = terms[0].split(' ')
		for term in terms:
			term = term.split("#")
			term = {'term':term[0], 'number':term[1]}
			list_terms.append(term)
		return list_terms

	def getTerms(self):
		list_terms = []
		for term in self.terms:
			list_terms.append(term["term"])
		return list_terms


def processSWNFile():
	# swn_dict = {}
	# for line in swn_file.readlines():
	# 	if "#" == line[0]:
	# 		continue
	# 	else:
	# 		line = line.split("\t")
	# 		try:
	# 			swn_dict[ int(line[1]) ] = SynSet(line[0], int(line[1]), float(line[2]),float(line[3]),line[4:-1],line[5])
	# 		except:
	# 			print "Linha: " + str(line)
	# 			print "Nao entendida."
	# return swn_dict
	for line in swn_file.readlines():
		double_vector = []
		if "#" == line[0]:
			continue

		data = line.split("\t")
		score = float(data[2]) - float(data[3])
		words = data[4].split(" ")
		for w in words:
			w_n = w.split("#")
			w_n[0] += "#" + data[0];
			index = int(w_n[1]) - 1

			if swn_dict.has_key(w_n[0]):
				v = swn_dict[w_n[0]]
				if index > (len(v)-1):
					for i in range(len(v)-1,index + 1):
						v.append(0.0)
				v[index] = score
			else:
				v = []
				for i in range(index + 1):
						v.append(0.0)
				v[index] = score
				swn_dict[w_n[0]] = v


def getSintaxe(tags):
	words = []
	for tag in tags:
		simple_tag = nltk.simplify_tag(tag[1])
		#Check Adverb
		if simple_tag == "ADV":
			words.append(tag[0] + "#r")
		#Check Noun
		elif simple_tag in ["N","FW","NP"]:
			words.append(tag[0] + "#n")
		#Check adjective
		elif simple_tag in ["ADJ","J"]:
			words.append(tag[0] + "#a")
		#Check verb
		elif simple_tag in ["V","VD","VG","VN"]:
			words.append(tag[0] + "#v")
	return words


def checkSentiment(tweet):
	tweet = tweet.lower()
	pos = 0
	neg = 0
	obj = 0

	final_score = 0

	words = nltk.word_tokenize(tweet)
	text = nltk.Text(words)
	tags = nltk.pos_tag(text)

	#Words in format word#a
	words = getSintaxe(tags)
	for word in words:
		word = word.lower()
		if swn_dict.has_key(word):
			v = swn_dict[word]
			score = 0
			sum_ = 0
			for i in range(len(v)):
				score += (float(1) / float((i+1))) * v[i]
			for i in range(1, len(v) + 1):
				sum_ += float(1) / float(i)
			score /= sum_
			final_score += score
	return final_score





# try:
# 	in_file = sys.argv[1]
# 	print "Path Twitter File: " + in_file
# 	in_file = open(in_file,"r")
# except:
# 	raise Exception("\nColoque o caminho para o arquivo dos tweets.\n\n")


#--SentiWordNet File
CURRENT_FILE_PATH = os.path.dirname(__file__)
if CURRENT_FILE_PATH != "":
    CURRENT_FILE_PATH += "/"

swn_file = CURRENT_FILE_PATH + "SentiWordNet_3.0.0_20130122.txt"
swn_file = open(swn_file,"r")
all_terms = {}
swn_dict = {}
processSWNFile()


if __name__ == '__main__':
    #####Getting Files
    #--Input Files
    mod = -1

    # MAIN : Running under all tweets
    TOTAL = 0
    TOTAL_POS = 0
    TOTAL_NEG = 0
    TOTAL_OBJ = 0

    for i in range(len(sys.argv)):
        if sys.argv[i] == "-f":
            in_file = sys.argv[i + 1]
            in_file = open(in_file,"r")
            mod = 0
            break
        if sys.argv[i] == "-t":
            tweet = sys.argv[i + 1]
            mod = 1
            break

        if (i + 1) == len(sys.argv):
            print "-t tweet \n \n or \n \n -f tweet_path_file"

    if mod == 0:
        for line in in_file.readlines():
            line.replace("\n","")
            sentiment = checkSentiment(line)
            print sentiment
    # MOD used by pyramid
    elif mod == 1:
        sentiment = checkSentiment(tweet)
        print sentiment
