#!/usr/bin/python
# coding: utf-8

from pattern.en import sentiment

if __name__ == "__main__":
    #--Input Files
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-t', '--text', dest='text')
    parser.add_option('-f', '--file', dest='file')
    options, args = parser.parse_args()
    
    threashold = 0.1
    positive = 1
    negative = -1
    neutral = 0

    if options.text:
        sent = sentiment(options.text)
        if sent[1] == 0.0:
                print neutral
        elif sent[0] >= threashold:
                print positive
        else:
                print negative

    elif options.file:
        with open(options.file,"rb") as in_file:
            for tweet in in_file:
                sent = sentiment(tweet)
                if sent[1] == 0.0:
                    print neutral
                elif sent[0] >= threashold:
                    print positive
                else:
                    print negative

    else:
    	print "Please use [-t|-f][text|filename]"


