import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

class NLPUtil {

	public static String[] tokenize(String text) {

		tokenize(text, " ");

		return null;
	}

	public static String[] tokenize(String text, String regularExpression) {

		return null;
	}

	/**
	 * List of text like "token1 token2"
	 */
	public static List<String> bigrams(String[] unigrams) {

		//Arrays.asList makes array and list "share" same elements (or elements area)
		return bigrams(Arrays.asList(unigrams));		
	}

	/**
	 * List of text like "token1 token2"
	 */
	public static List<String> bigrams(List<String> unigrams) {

		List<String> bigrams = new ArrayList<>(unigrams.size()); 

		for (int i=0; i < unigrams.size() - 1; ++i) {

			bigrams.add(unigrams.get(i) + " " + unigrams.get(i + 1));
		}

		return bigrams;
	}
}

/**
 * @author jpaulo
 * Hashtag Lexicon, by Mohammed
 */
public class NRC_HashtagPaper {

	private Map<String, Double> unigramsDictionary;
	private Map<String, Double> bigramsDictionary;
	private Map<String, Double> pairsDictionary;
	private String unigramsDictionaryFilePath;
	private String bigramsDictionaryFilePath;
	private String pairsDictionaryFilePath;

	public NRC_HashtagPaper(String unigramsDictionaryFilePath, 
			String bigramsDictionaryFilePath, String pairsDictionaryFilePath) {

		this.unigramsDictionaryFilePath = unigramsDictionaryFilePath;
		this.bigramsDictionaryFilePath = bigramsDictionaryFilePath;
		this.pairsDictionaryFilePath = pairsDictionaryFilePath;
		this.loadDictionaries();
	}

	/**
	 * @author jpaulo
	 */
	public void loadDictionaries() {

		this.unigramsDictionary = new HashMap<>();			
		this.bigramsDictionary = new HashMap<>();			
		this.pairsDictionary = new HashMap<>();			

		loadDictionary(this.unigramsDictionary, this.unigramsDictionaryFilePath);
		loadDictionary(this.bigramsDictionary, this.bigramsDictionaryFilePath);
		loadDictionary(this.pairsDictionary, this.pairsDictionaryFilePath);
	}

	/**
	 * @author jpaulo
	 */
	private void loadDictionary(Map<String, Double> dictionary, String dictionaryFilePath) {

		try {
			//			int cont = 1;
			BufferedReader br = new BufferedReader(new FileReader(dictionaryFilePath));
			String line = br.readLine();

			while (line != null) {

				String[] data = line.split("\t"); //[0]: sentiment ngram; [1]: sentiment score

				if (!dictionary.containsKey(data[0])) {
					dictionary.put(data[0], Double.valueOf(data[1]));
				}
				//				else {
				//					System.out.println(cont + ") " + line);
				//					break;
				//				}
				/* Errors with special characters in files:
unigrams file, line 313: ????????????	5	5	0
bigrams file, line 2629: ? ????	5	6	0
pairs file, line 20303: good---???	5	16	2
				 */
				//				++cont;
				line = br.readLine();
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Algorithm: 
	 * look for all pairs and sum its scores;
	 * remove all unigrams and bigrams founded by pairs;
	 * look for all bigrams;
	 * remove all unigrams founded in bigrams.
	 *
	 * SEE http://stackoverflow.com/questions/25377666/calculating-the-sentiment-score-of-a-sentence-using-ngrams
	 * Example sum bigrams and unigrams scores, without removing any.
	 */
	public int analyseText(String text) {

		double scoreTotal = 0d;
		String[] tokens = text.toLowerCase().split(" ");
		List<String> unigrams = Arrays.asList(tokens);
		List<String> bigrams = NLPUtil.bigrams(unigrams);

		//tratar stop words? Provavelmente nao, pois afetaria os bigrams & pairs
		//tratar pontuacao?

		//pairs (paper text: "480,010 non-contiguous pairs."). 
		int qtUnigrams = unigrams.size();
		int qtBigrams  = bigrams.size();

		Set<Integer> unigramsIndexesToRemove = new HashSet<Integer>();
		Set<Integer> bigramsIndexesToRemove  = new HashSet<Integer>();

		//pairs unigram-unigram
		scoreTotal += this.pairsAux(unigrams, 0, qtUnigrams - 2, unigramsIndexesToRemove,
				unigrams, 2, qtUnigrams, unigramsIndexesToRemove);

		//pairs unigram-bigram
		scoreTotal += this.pairsAux(unigrams, 0, qtUnigrams - 2, unigramsIndexesToRemove,
				bigrams, 2, qtBigrams, bigramsIndexesToRemove);

		//pairs bigram-unigram
		scoreTotal += this.pairsAux(bigrams, 0, qtBigrams - 2, bigramsIndexesToRemove, 
				unigrams, 3, qtUnigrams, unigramsIndexesToRemove);

		//pairs bigram-bigram
		scoreTotal += this.pairsAux(bigrams, 0, qtBigrams - 2, bigramsIndexesToRemove, 
				bigrams, 3, qtBigrams, bigramsIndexesToRemove);

		//remove bigrams found in pairs from bigrams list
		this.removeElementsByIndexes(bigrams, bigramsIndexesToRemove);

		//bigrams
		int i = 0;
		for (String bigram : bigrams) {

			if (bigram != null && this.bigramsDictionary.containsKey(bigram)) {
				//System.out.println("bigram found: [" + i + "]" + bigram + " = " + this.bigramsDictionary.get(bigram));
				scoreTotal += this.bigramsDictionary.get(bigram);

				//add indexes to remove elements in unigrams
				unigramsIndexesToRemove.add(i);
				unigramsIndexesToRemove.add(i+1);
			}

			++i;
		}

		this.removeElementsByIndexes(unigrams, unigramsIndexesToRemove);

		//unigrams
		i = 0;
		for (String unigram : unigrams) {
			if (unigram != null && this.unigramsDictionary.containsKey(unigram)) {
				//System.out.println("unigram found: [" + i + "]" + unigram + " = " + this.unigramsDictionary.get(unigram));
				scoreTotal += this.unigramsDictionary.get(unigram);
			}
			++i;
		}

		//System.out.println("Score of sentence '" + text + "': " + scoreTotal);

		double threshold = 0.5; //1d, 0.75 ?? 

		if (scoreTotal > threshold) {
			return 1;
		}
		else if (scoreTotal < -threshold) {
			return -1;
		}

		return 0; //neutral OR N/A
	}

	/**
	 * @param ngramsList (uni- or bi-grams list)
	 * @param ngramsIndexesToRemove
	 * Set null to all listed indexes.
	 */
	private void removeElementsByIndexes(List<String> ngramsList, Set<Integer> ngramsIndexesToRemove) {

		for (int index : ngramsIndexesToRemove) {

			ngramsList.set(index, null);
			//System.out.print(index + " ");
		}
	}

	/**
	 * @param list1 (uni- or bi-grams)
	 * @param list2 (uni- or bi-grams)
	 * I- inclusive indexation ; F- exclusive indexation
	 * @return score/strength of all pairs formed by lists' elements, given indexes
	 */
	private double pairsAux(List<String> list1, int index1I, int index1F, Set<Integer> listIndexesToRemove1,
			List<String> list2, int index2I, int index2F, Set<Integer> listIndexesToRemove2) {

		double scoreTotal = 0d;		
		for (int i1=index1I; i1 < index1F; ++i1) {

			for (int i2=index2I + i1; i2 < index2F; ++i2) {

				String ngram1 = list1.get(i1);
				String ngram2 = list2.get(i2);
				String pair = new String(ngram1 + "---" + ngram2);

				if (this.pairsDictionary.containsKey(pair)) {
					//System.out.println("Pair found: [" + i1 + "]" + pair + "[" + i2 + "] = " + this.pairsDictionary.get(pair));
					scoreTotal += this.pairsDictionary.get(pair);

					//set indexes to remove elements from bigrams and unigrams lists
					listIndexesToRemove1.add(i1);
					listIndexesToRemove2.add(i2);
				}
			}
		}

		return scoreTotal;
	}

	public void analyseFile(String filePath) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));

			String line = br.readLine();
			while (line != null) {
				System.out.println(this.analyseText(line));
				line = br.readLine();
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		String baseDir = "./";
		String argTextOrFile = args[1];
		NRC_HashtagPaper method = new NRC_HashtagPaper(baseDir + "unigrams-pmilexicon.txt", 
				baseDir + "bigrams-pmilexicon.txt", baseDir + "pairs-pmilexicon.txt");

		switch (args[0]) {
		case "-t":
			System.out.println(method.analyseText(argTextOrFile));
			break;
		case "-f":
			method.analyseFile(argTextOrFile);
			break;
		} 

	}
}
