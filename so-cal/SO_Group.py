### SO_Group.py by Julian Brooke (September 2008)
### see README for more information

import sys
import os

config = "SO_Calc.ini"

g_num = int(sys.argv[2][-1])
cutoffs = [-9999]
g_directories = []
output_dir = sys.argv[1]

for i in range(3,g_num + 2):
    cutoffs.append(float(sys.argv[i]))
cutoffs.append(9999)

for i in range(g_num + 2, g_num*2 + 2):
    g_directories.append(sys.argv[i])

results = []

current_dir = os.getcwd()
if not os.path.exists (current_dir + "/" + output_dir):
    os.mkdir(output_dir)

for directory in g_directories:
    print "Searching " + directory
    sub_output_dir = current_dir + "/" + output_dir + "/" + directory.split("/")[-1]
    if not os.path.exists ( sub_output_dir):
        os.mkdir(sub_output_dir)
    in_files = os.listdir(directory)
    output = sub_output_dir + "/output.txt"
    if os.path.exists (output):
        os.remove(output)
    if os.path.exists (sub_output_dir + "/richout.txt"):
        os.remove(sub_output_dir + "/richout.txt")
    if os.path.exists (sub_output_dir + "/search.txt"):
        os.remove(sub_output_dir + "/search.txt")
    for filename in in_files:
        f = open(output, "a")
        print "Processing " + filename
        f.close()
        input_dir = current_dir + "/" + directory
        cmd = "python SO_Calc.py " +config + " " + input_dir + "/" + filename + " " + output + " " + sub_output_dir + "/richout.txt "+ sub_output_dir + "/" + "search.txt"
        os.system(cmd)
    f = open(output, "r")
    result = []
    for i in range(g_num):
        result.append(0)
    for line in f.read().split("\n"):
        if line:
            (filename, SO) = line.split("\t")
            SO = float(SO)
            for i in range(g_num):
                if SO > cutoffs[i] and SO < cutoffs[i+1]:
                    result[i] += 1
            f.close()
    results.append(result)
            
f = open(output_dir + "/finalout.txt", "w")
sum_total = 0
total_correct = 0
total_close = 0
total_result = []
for i in range(g_num):
    total_result.append(0)
for i in range(g_num):
    f.write ("---------------\nResults for Group " +str(i+1)+ "\n---------------\n")
    string = ""
    for j in range(g_num):
        string += "\tGroup " + str(j+1)
    f.write (string + "\n")
    string = ""
    for j in range(g_num):
        string += "\t" +str(results[i][j])
        total_result[j] += results[i][j]
    f.write(string + "\n")
    total = sum(results[i])
    f.write("Percent Correct: " + str(100.0*results[i][i]/total) + " %\n")
    if i == 0:
        close = results[i][0] + results[i][1]
    elif i == g_num -1:
        close = results[i][i] + results[i][i-1]
    else:
        close = results[i][i] + results[i][i-1] + results[i][i+1]
    f.write ("Percent Close: " + str(100.0*close/total) + " %\n")
    sum_total += total
    total_correct += results[i][i]
    total_close += close
f.write ("-----\nOverall:\n-----\n")
string = ""
for j in range(g_num):
    string += "\tGroup " + str(j+1)
f.write (string + "\n")
string = ""
for j in range(g_num):
    string += "\t" +str(total_result[j])
f.write(string + "\n")
f.write ("Percent Correct: " + str(100.0*total_correct/sum_total) + " %\n")
f.write ("Percent Close: " + str(100.0*total_close/sum_total) + " %\n")
f.close()




    

