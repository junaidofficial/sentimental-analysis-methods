# get_stats.py by Julian Brooke(Januray 2009)
# This program takes a rich output file from SO-CAL (V1.11) and outputs a file
# with relevant statistics 

import sys
in_file = open(sys.argv[1])
out_file = open(sys.argv[2], "w")
num_texts = 0
total_text_length = 0
total_SO_words = 0
total_SO_words_used =0
total_SO_value = 0
total_abs_SO_value = 0
total_abs_word_SO_value = 0
count = {}
count["int"] = 0
count["comp"] =0
count["super"] =0
count["negw"] = 0
count["repeat"] = 0
count["block"] = 0
count["weight"] = 0
count["neg"] = 0
count["neg_neg"] = 0
count["capital"] = 0
count["exclaim"] = 0
count["highlight"] = 0
count["question"] = 0
count["imperative"] = 0
count["quote"] = 0
count["irrealis"] = 0
count["int_amount"] = 0
count["abs_SO_value"] = 0
count["abs_base_SO_value"] = 0
count["ones"]= 0
count["twos"] = 0
count["threes"]= 0
count["fours"] = 0
count["fives"] = 0
count["positive"] = 0
count["negative"] = 0
total_noun_count = 0
total_verb_count = 0
total_adj_count = 0
total_adv_count = 0
within_2 = 0
within_5 = 0
within_10 = 0
within_15 = 0
within_20 = 0

def get_counts(output):
    full_count = 0
    zero_count = 0
    for line in output.split("\n"):
        if line:
            full_count +=1
            parts = line.split(" ")
            SO_value = float(parts[-1])
            count["abs_SO_value"] += abs(SO_value)
            i = 0
            while parts[i][-2:] != ".0":
                i += 1
            word_SO = float(parts[i])
            if abs(word_SO) == 1:
                count["ones"] +=1
            if abs(word_SO) == 2:
                count["twos"] +=1
            if abs(word_SO) == 3:
                count["threes"] +=1
            if abs(word_SO) == 4:
                count["fours"] +=1
            if abs(word_SO) == 5:
                count["fives"] +=1
            if word_SO >= 0:
                count["positive"] += 1
            else:
                count["negative"] += 1
                count["neg_neg"] += line.count("(NEGATED)")
            count["abs_base_SO_value"] += abs(word_SO) 
            while i < len(parts) and parts[i] != "(INTENSIFIED)":
                i += 1
            if i != len(parts):
                count["int_amount"] += abs((float(parts[i -1]) - 1) * word_SO)
            if SO_value == 0:
                zero_count +=1
            count["neg"] += line.count("(NEGATED)")
            count["comp"] += line.count("(COMPARATIVE)")
            count["super"] += line.count("(SUPERLATIVE")
            count["int"] += line.count("(INTENSIFIED)")
            count["negw"] += line.count("(NEGATIVE)")
            count["repeat"] += line.count("(REPEATED)")
            count["block"] += line.count("(BLOCKED)")
            count["weight"] += line.count("(WEIGHTED)")
            count["capital"] += line.count("(CAPITALIZED)")
            count["exclaim"] += line.count("(EXCLAMATION)")
            count["highlight"] += line.count("(HIGHLIGHTED)")
            count["question"] += line.count("(QUESTION)")
            count["quote"] += line.count("(QUOTES)")
            count["imperative"] += line.count("(IMPERATIVE)")
            count["irrealis"] += line.count("(IRREALIS)")
    return [full_count, zero_count]
    

for text_info in in_file.read().split("######"):
    if text_info:
        num_texts +=1
        len_index = text_info.find("Text Length: ")
        total_text_length += int(text_info[(len_index + 13):(text_info.find("\n", len_index))])
        SO_index = text_info.find("Total SO: ")
        SO_value = float(text_info[(SO_index + 10):(text_info.find("\n", SO_index))])
        total_SO_value += SO_value
        start = text_info.find("Nouns:\n-----") + 12
        end = text_info.find("\n-----", start)
        (noun_total, noun_zero) = get_counts(text_info[start:end])
        noun_count = noun_total - noun_zero
        total_noun_count += noun_total
        start = text_info.find("Verbs:\n-----") + 12
        end = text_info.find("\n-----", start)
        (verb_total, verb_zero) = get_counts(text_info[start:end])
        verb_count = verb_total - verb_zero
        total_verb_count += verb_total
        start = text_info.find("Adjectives:\n-----") + 17
        end = text_info.find("\n-----", start)
        (adj_total, adj_zero) = get_counts(text_info[start:end])
        adj_count = adj_total - adj_zero
        total_adj_count += adj_total
        start = text_info.find("Adverbs:\n-----") + 14
        end = text_info.find("\n-----", start)
        (adv_total, adv_zero) = get_counts(text_info[start:end])
        adv_count = adv_total - adv_zero
        total_adv_count += adv_total
        total_SO_words += noun_total + verb_total + adj_total + adv_total
        SO_words_used = noun_count + verb_count + adj_count +adv_count
        total_SO_words_used += SO_words_used
        dist_from_zero = abs(SO_words_used*SO_value)
        if dist_from_zero <= 2:
            within_2 += 1
        if dist_from_zero <= 5:
            within_5 += 1
        if dist_from_zero <=10:
            within_10 += 1
        if dist_from_zero <=15:
            within_15 += 1
        if dist_from_zero <=20:
            within_20 += 1
avg_text_length = float(total_text_length) / num_texts
avg_SO_words = float(total_SO_words)/ num_texts
avg_SO_words_used = float(total_SO_words_used)/ num_texts
avg_nouns = float(total_noun_count)/total_SO_words
avg_verbs = float(total_verb_count)/total_SO_words
avg_adj = float(total_adj_count)/total_SO_words
avg_adv = float(total_adv_count)/total_SO_words
avg_SO = float(total_SO_value)/num_texts
avg_abs_SO_value = count["abs_SO_value"]/total_SO_words
avg_abs_base_SO_value = count["abs_base_SO_value"]/total_SO_words
avg_ones = count["ones"]/float(total_SO_words)
avg_twos = count["twos"]/float(total_SO_words)
avg_threes = count["threes"]/float(total_SO_words)
avg_fours = count["fours"]/float(total_SO_words)
avg_fives = count["fives"]/float(total_SO_words)
avg_positive = count["positive"]/float(total_SO_words)
avg_negative = count["negative"]/float(total_SO_words)
avg_intensification = count["int_amount"]/count["int"]
avg_negative_negation = count["neg_neg"]/float(count["neg"])
out_file.write("No. of Texts\t" + str(num_texts) +"\n")
out_file.write("Total No. of SO-valued words\t" + str(total_SO_words) + "\n")
out_file.write("Average Text SO Value\t" + str(avg_SO) + "\n")
out_file.write("Percentage of Nouns\t" + str(avg_nouns) + "\n")
out_file.write("Percentage of Verbs\t" + str(avg_verbs) + "\n")
out_file.write("Percentage of Adjectives\t" + str(avg_adj) + "\n")
out_file.write("Percentage of Adverbs\t" + str(avg_adv) + "\n")
out_file.write("Average Absolute Word SO Value\t" + str(avg_abs_base_SO_value) + "\n")
out_file.write("Average Absolute Modified SO Value\t"+ str(avg_abs_SO_value) + "\n")
out_file.write("Percentage of words with absolute SO value of 1\t" + str(avg_ones) + "\n")
out_file.write("Percentage of words with absolute SO value of 2\t" + str(avg_twos) + "\n")
out_file.write("Percentage of words with absolute SO value of 3\t" + str(avg_threes) + "\n")
out_file.write("Percentage of words with absolute SO value of 4\t" + str(avg_fours) + "\n")
out_file.write("Percentage of words with absolute SO value of 5\t" + str(avg_fives) + "\n")
out_file.write("Percentage of positive words\t" +str(avg_positive) + "\n")
out_file.write("Percentage of negative words\t" +str(avg_negative) + "\n")
out_file.write("Average SO Change per Intensification\t" + str(avg_intensification) + "\n")
out_file.write("Percentage of Negation involving Negative SO words\t" + str(avg_negative_negation) + "\n")
out_file.write("Average No. of Intensifications per Text\t" + str(count["int"]/float(num_texts)) + "\n")
out_file.write("Average No. of Negations per Text\t" + str(count["neg"]/float(num_texts)) + "\n")
out_file.write("Average No. of Comparatives per Text\t" + str(count["comp"]/float(num_texts)) + "\n")
out_file.write("Average No. of Superatives per Text\t" + str(count["super"]/float(num_texts)) + "\n")
out_file.write("Average No. of Exclamations per Text\t" + str(count["exclaim"]/float(num_texts)) + "\n")
out_file.write("Average No. of Capitalizations per Text\t" + str(count["capital"]/float(num_texts)) + "\n")
out_file.write("Average No. of Highlights per Text\t" + str(count["highlight"]/float(num_texts)) + "\n")
out_file.write("Average No. of Negative Expressions per Text\t" + str(count["negw"]/float(num_texts)) + "\n")
out_file.write("Average No. of Repeated SO Words per Text\t" + str(count["repeat"]/float(num_texts)) + "\n")
out_file.write("Average No. of Irrealis Blocks per Text\t" + str(count["irrealis"]/float(num_texts)) + "\n")
out_file.write("Average No. of Question Blocks per Text\t" + str(count["question"]/float(num_texts)) + "\n")
out_file.write("Average No. of Quote Blocks per Text\t" + str(count["quote"]/float(num_texts)) + "\n")
out_file.write("Average No. of Imperative Blocks per Text\t" + str(count["imperative"]/float(num_texts)) + "\n")
out_file.write("Average No. of Modifer Blocks per Text\t" + str(count["block"]/float(num_texts)) + "\n")
out_file.write("Average No. of Weighted Words per Text\t" + str(count["weight"]/float(num_texts)) + "\n")
out_file.write("Average No. of Tokens per Text\t" +str(avg_text_length) +"\n")
out_file.write("Average No. of SO-carrying Words per Text\t" + str(avg_SO_words) + "\n")
out_file.write("Average No. of Non-zero Expressions per Text\t" +str(avg_SO_words_used) + "\n")
out_file.write("Percentage of Texts within 2 SO of Polarity Cutoff\t" +str(within_2/float(num_texts)) + "\n")
out_file.write("Percentage of Texts within 5 SO of Polarity Cutoff\t" +str(within_5/float(num_texts)) + "\n")
out_file.write("Percentage of Texts within 10 SO of Polarity Cutoff\t" +str(within_10/float(num_texts)) + "\n")
out_file.write("Percentage of Texts within 15 SO of Polarity Cutoff\t" +str(within_15/float(num_texts)) + "\n")
out_file.write("Percentage of Texts within 20 SO of Polarity Cutoff\t" +str(within_20/float(num_texts)))

out_file.close()
in_file.close()
