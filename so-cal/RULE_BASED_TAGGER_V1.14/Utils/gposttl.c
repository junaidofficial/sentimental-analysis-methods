/* This is a fork by Kyle Gorman <gormanky@ohsu.edu>, Copyright 2011
___________________________________________________________________

This file is a part of GPoSTTL: An enhanced version of Brill's
rule-based Parts-of-Speech Tagger with built-in Tokenizer and
Lemmatizer by Golam Mortuza Hossain <gmhossain at gmail.com>.
This file can be redistributed and/or modified under the terms of
either the license of GPoSTTL or optionally under GPL as below.
___________________________________________________________________
 
Copyright (C) 2007-2008, Golam Mortuza Hossain <gmhossain@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public
License along with this program; if not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
Boston, MA 02110-1301, USA.
__________________________________________________________________

*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "tagger_controller.h"
#include "lex.h"
#include "darray.h"
#include "useful.h"
#include "memory.h"
#include "tagger.h"
#include "tokenizer.h"
#include "enhance_penntag.h"

#define BUFLEN 102400 // KG

// heavy modification by KG
int initialize_tagger(void) {

	char basepath[250] = "/usr/local/share/gposttl";
	int i = _initialize(basepath);
	if (i == 0)
		return 0;
	return -1;
}

char *check_and_tag(char ptr0[], int enhance_penntag) {
    return tag(Tokenizer(ptr0), enhance_penntag);
}
// end heavy modification

int main(int argc, char *argv[]) {

	static int enhance_penntag = 1;
    static int silent, version = 0;
	int ch, ch2, i = 0, n = 1, j = 1;
	char *buf, *inptr, *ptr0, *ptr1, *ptr2, *ptr3;
	FILE *fp;

    // read command line options using getopt
	if (argc > 4) {
		fprintf(stderr,
			"error!!usage: %s [OPTIONS] [FILE or STDIN]\n",
			argv[0]);
		exit(1);
	}

	while (1) {
		static struct option long_options[] = {
			{"verbose", no_argument, &silent, 0},
			{"brill-mode", no_argument, &enhance_penntag, 0},
			{"silent", no_argument, &silent, 1},
			{"version", no_argument, &version, 1},
			{0, 0, 0, 0}
		};
		// getopt_long stores the option index here.
		int c, option_index = 0;

		c = getopt_long(argc, argv, "abc:d:f:",
				long_options, &option_index);

		// detect the end of options
		if (c == -1)
			break;

		switch (c) {
		case 0:
            // if the option set a flag, do nothing else
			if (long_options[option_index].flag != 0)
				break;
		case '?':
			// getopt_long already printed an error message.
			break;

		default:
			abort();
		}
	}

    // print tagger info and quit */
	if (version) {
		fprintf(stdout, "GPoSTTL %s", VERSION);
		exit(0);
	}

	/* If input filename has been specified then use it
	   otherwise use stdin for input. */
	fp = stdin;

	if (optind < argc) {
		fp = fopen(argv[optind++], "r");
		if (fp == NULL) {
			fprintf(stderr, "error!! File %s not found\n",
				argv[i]);
			exit(1);
		}
	}
    // end of getopt

    //	initialize
	if (initialize_tagger() != 0) {
		fprintf(stderr, "Initialization failed! Datafiles are missing. ");
		fprintf(stderr, "If you have\nthe datafiles in a ");
		fprintf(stderr, "non-standard location then please set\n");
		fprintf(stderr, "the environment variable GPOSTTL_DATA_DIR.\n");
		exit(1);
	}

    // processing 
    // modified by KG
    char* line = (char *) malloc(sizeof(char) * BUFLEN); 
    while (fgets(line, BUFLEN, fp) != NULL) {
        line[strlen(line) - 1] = ' '; // for the tokenizer
        fprintf(stdout, "%s\n", check_and_tag(line, enhance_penntag));
    }
    // end modification by KG

    // Destroy tagger */
	destroy_tagger();
	free(line);

	if (!silent)
		fprintf(stderr, "GPoSTTL: Done\n");

	return 0;
}
