#!/usr/bin/perl -w
use strict;

my $arg1; #-t ou -f
my $arg2; #tweet ou arquivo
my $c = 0;
our $text;

open(IN, "unigrams-pmilexicon.txt") or die "could not open unigrams-pmilexicon.txt\n";

our %NRC = ();

while (my $line = <IN>) 
{
    chomp $line;
    
    if( $line =~ /([^<]+)/)
	{        
        my @array = split(" ", $1);
        
        my $word = $array[0];
        my $score = $array[1];     

        if(not exists $NRC{$word})
        {
            $NRC{$word} = $score;
        }       
                 
	}
}

foreach my $argnum (0 .. $#ARGV)
{
    if($c == 0)
    {
         $arg1 = $ARGV[$argnum];
         $c+=1;
    }
    elsif($c == 1)
    {
        $arg2 = $ARGV[$argnum];
        $c+=1;
    }
}

if($arg1 eq "-t")
{
    $text = $arg2;
    &trataTweet;
 }
elsif($arg1 eq "-f")
{
    &abreArqs;
}

sub trataTweet
{
    my $score_total = 0;
        my @words = split(" ", $text);
       
        foreach(@words)
        {
	    my $w = $_;
            if(exists $NRC{$w})
            {
                $score_total+=$NRC{$w};
            }
        }
        
        if($score_total > 0)
        {
            print "1\n";
        }
        
        elsif($score_total < 0)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        }   
}

sub abreArqs
{
    open(IN2, "$arg2") or die "could not open $arg2\n";

    while (my $line = <IN2>) 
    {
        chomp $line;
        
        my $score_total = 0;
        my @words = split(" ", $line);
       
        foreach(@words)
        {
	   my $w = $_;
            if(exists $NRC{$w})
            {
                $score_total+=$NRC{$_};
            }
        }
        
        if($score_total > 0)
        {
            print "1\n";
        }
        
        elsif($score_total < 0)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        }   
    }
}

close IN;
close IN2;

