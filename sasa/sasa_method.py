#!/usr/bin/python
import sasa
from sasa.classifier import Classifier
sasaClassifier = Classifier()

#####Getting Files
#Used on iFeel
def checkText(text):
	checkText = sasaClassifier.classifyFromText
	return checkText(text)[1]

if __name__ == "__main__":
    #--Input Files
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-t', '--text', dest='text')
    parser.add_option('-f', '--file', dest='file')
    options, args = parser.parse_args()


    if options.text:
    	text = options.text
    	print checkText(text)

    elif options.file:
    	with open(options.file,"rb") as infile:
    	    for line in infile:
        		print checkText(line)



