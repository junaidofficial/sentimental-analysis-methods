output_f = open("emoticon_ds_polarized.txt","w")

with open("emoticon.words.advanced","r") as f:
	for line in f:
		line = line.replace("\n","").split("\t")
		pos = int(line[0])
		neg = int(line[1])
		result = 1 if pos > neg else -1 if pos < neg else 0
		word = line[2]
		output_f.write(word + "\t" + str(result) + "\n")

output_f.close()
