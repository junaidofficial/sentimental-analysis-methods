#!/usr/bin/perl -w
use strict;

my $arg1; #-t ou -f
my $arg2; #tweet ou arquivo
my $c = 0;
our $text;

foreach my $argnum (0 .. $#ARGV)
{
    if($c == 0)
    {
         $arg1 = $ARGV[$argnum];
         $c+=1;
    }
    elsif($c == 1)
    {
        $arg2 = $ARGV[$argnum];
        $c+=1;
    }
}

open(IN, "./corpus.txt") or die "could not open corpus.txt\n";

our %NRC = ();

while (my $line = <IN>) 
{
    chomp $line;

    if( $line =~ /([^<]+)/)
    {
        my @array = split(" ", $1);
        
        my $sentimento = $array[0];
        my $word = lc $array[1];
        my $value = $array[2];
        
        if($sentimento eq 'anticipation')
        {
            if(not exists $NRC{$word}{'anticipation'})
            {
                $NRC{$word}{'anticipation'} = $value;
            }
            else
            {
                $NRC{$word}{'anticipation'} += $value;
            }
        }
        
        elsif($sentimento eq 'fear')
        {        
            if($sentimento eq 'fear')
            {
                if(not exists $NRC{$word}{'fear'})
                {
                    $NRC{$word}{'fear'} = $value;
                }
                else
                {
                    $NRC{$word}{'fear'} += $value;
                }
            }       
        }
        
        elsif($sentimento eq 'anger')
        {
            if($sentimento eq 'anger')
            {
                if(not exists $NRC{$word}{'anger'})
                {
                    $NRC{$word}{'anger'} = $value;
                }
                else
                {
                    $NRC{$word}{'anger'} += $value;
                }
            }  
        }
        
        elsif($sentimento eq 'trust')
        {
            if($sentimento eq 'trust')
            {
                if(not exists $NRC{$word}{'trust'})
                {
                    $NRC{$word}{'trust'} = $value;
                }
                else
                {
                    $NRC{$word}{'trust'} += $value;
                }
            }  
        }
        
        elsif($sentimento eq 'surprise')
        {
            if($sentimento eq 'surprise')
            {
                if(not exists $NRC{$word}{'surprise'})
                {
                    $NRC{$word}{'surprise'} = $value;
                }
                else
                {
                    $NRC{$word}{'surprise'} += $value;
                }
            } 
        }
        
        elsif($sentimento eq 'sadness')
        {
            if($sentimento eq 'sadness')
            {
                if(not exists $NRC{$word}{'sadness'})
                {
                    $NRC{$word}{'sadness'} = $value;
                }
                else
                {
                    $NRC{$word}{'sadness'} += $value;
                }
            } 
        }
        
        elsif($sentimento eq 'joy')
        {
            if($sentimento eq 'joy')
            {
                if(not exists $NRC{$word}{'joy'})
                {
                    $NRC{$word}{'joy'} = $value;
                }
                else
                {
                    $NRC{$word}{'joy'} += $value;
                }
            } 
        }
        
        elsif($sentimento eq 'disgust')
        {
            if($sentimento eq 'disgust')
            {
                if(not exists $NRC{$word}{'disgust'})
                {
                    $NRC{$word}{'disgust'} = $value;
                }
                elsif(exists $NRC{$word}{'disgust'})
                {
                    $NRC{$word}{'disgust'} += $value;
                }
            }
        }         
    }
}

if($arg1 eq "-t")
{
    $text = $arg2;
    &trataTweet;
 }
elsif($arg1 eq "-f")
{
    &abreArqs;
}

sub trataTweet
{
    my @words = split(" ", $text);
       
        my $ang; 
        my $anti;
        my $disg;
        my $fea;
        my $jo;
        my $sad;
        my $sur;
        my $trus;
        
        my $pos_total = 0;
        my $neg_total = 0;
        my $neut_total = 0;
        
        foreach(@words)
        {
		my $w = lc $_;
            if(exists $NRC{$w})
            {
                if(not exists $NRC{$w}{'anger'})
                {
                    $ang = 0;
                }
                
                elsif(exists $NRC{$w}{'anger'})
                {
                    $ang = $NRC{$w}{'anger'};
                }
                
                if(not exists $NRC{$w}{'anticipation'})
                {
                    $anti = 0;
                }
                
                elsif(exists $NRC{$w}{'anticipation'})
                {
                    $anti = $NRC{$w}{'anticipation'};
                }
                
                if(not exists $NRC{$w}{'fear'})
                {
                    $fea = 0;
                }
                
                elsif(exists $NRC{$w}{'fear'})
                {
                    $fea = $NRC{$w}{'fear'};
                }
                
                if(not exists $NRC{$w}{'disgust'})
                {
                    $disg = 0;
                }
                
                elsif(exists $NRC{$w}{'disgust'})
                {
                    $disg = $NRC{$w}{'disgust'};
                }
                
                if(not exists $NRC{$w}{'joy'})
                {
                    $jo = 0;
                }
                
                elsif(exists $NRC{$w}{'joy'})
                {
                    $jo = $NRC{$w}{'joy'};
                }
                
                if(not exists $NRC{$w}{'sadness'})
                {
                    $sad = 0;
                }
                
                elsif(exists $NRC{$w}{'sadness'})
                {
                    $sad = $NRC{$w}{'sadness'};
                }
                
                if(not exists $NRC{$w}{'surprise'})
                {
                    $sur = 0;
                }
                
                elsif(exists $NRC{$w}{'surprise'})
                {
                    $sur = $NRC{$w}{'surprise'};
                }
                
                if(not exists $NRC{$w}{'trust'})
                {
                    $trus = 0;
                }
                
                elsif(exists $NRC{$w}{'trust'})
                {
                    $trus = $NRC{$w}{'trust'};
                }
                
                $pos_total += $jo;
                $pos_total += $trus;
                
                $neg_total += $ang;
                $neg_total += $disg;
                $neg_total += $fea;
                $neg_total += $sad;
                
                $neut_total += $sur;
                $neut_total += $anti;
                
            }
        }
        
        if($pos_total > $neg_total && $pos_total > $neut_total)
        {
            print "1\n";
        }
        
        elsif($neg_total > $pos_total && $neg_total > $neut_total)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        } 
}

sub abreArqs
{
    open(IN2, "$arg2") or die "could not open $arg2\n";

    while (my $line = <IN2>) 
    {
        chomp $line;
        
        my @words = split(" ", $line);
       
        my $ang; 
        my $anti;
        my $disg;
        my $fea;
        my $jo;
        my $sad;
        my $sur;
        my $trus;
        
        my $pos_total = 0;
        my $neg_total = 0;
        my $neut_total = 0;
        
        foreach(@words)
        {
            if(exists $NRC{$_})
            {
                if(not exists $NRC{$_}{'anger'})
                {
                    $ang = 0;
                }
                
                elsif(exists $NRC{$_}{'anger'})
                {
                    $ang = $NRC{$_}{'anger'};
                }
                
                if(not exists $NRC{$_}{'anticipation'})
                {
                    $anti = 0;
                }
                
                elsif(exists $NRC{$_}{'anticipation'})
                {
                    $anti = $NRC{$_}{'anticipation'};
                }
                
                if(not exists $NRC{$_}{'fear'})
                {
                    $fea = 0;
                }
                
                elsif(exists $NRC{$_}{'fear'})
                {
                    $fea = $NRC{$_}{'fear'};
                }
                
                if(not exists $NRC{$_}{'disgust'})
                {
                    $disg = 0;
                }
                
                elsif(exists $NRC{$_}{'disgust'})
                {
                    $disg = $NRC{$_}{'disgust'};
                }
                
                if(not exists $NRC{$_}{'joy'})
                {
                    $jo = 0;
                }
                
                elsif(exists $NRC{$_}{'joy'})
                {
                    $jo = $NRC{$_}{'joy'};
                }
                
                if(not exists $NRC{$_}{'sadness'})
                {
                    $sad = 0;
                }
                
                elsif(exists $NRC{$_}{'sadness'})
                {
                    $sad = $NRC{$_}{'sadness'};
                }
                
                if(not exists $NRC{$_}{'surprise'})
                {
                    $sur = 0;
                }
                
                elsif(exists $NRC{$_}{'surprise'})
                {
                    $sur = $NRC{$_}{'surprise'};
                }
                
                if(not exists $NRC{$_}{'trust'})
                {
                    $trus = 0;
                }
                
                elsif(exists $NRC{$_}{'trust'})
                {
                    $trus = $NRC{$_}{'trust'};
                }
                
                $pos_total += $jo;
                $pos_total += $trus;
                
                $neg_total += $ang;
                $neg_total += $disg;
                $neg_total += $fea;
                $neg_total += $sad;
                
                $neut_total += $sur;
                $neut_total += $anti;
                
            }
        }
        
        if($pos_total > $neg_total && $pos_total > $neut_total)
        {
            print "1\n";
        }
        
        elsif($neg_total > $pos_total && $neg_total > $neut_total)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        }   
    }
}

close IN;
close IN2;

