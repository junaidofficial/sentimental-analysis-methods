**Umigon**
======

*Semantic analysis on Twitter*

Try Umigon at [www.umigon.com](http://www.umigon.com)  

Read the tech paper [here](http://www.clementlevallois.net/download/umigon.pdf).

[Contact me](https://twitter.com/seinecle)!