#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from subprocess import PIPE, Popen, STDOUT
from threading  import Thread

def print_output(out, ntrim=80):
    for line in out:
        print line
        sys.exit(1)


if __name__=="__main__":
    p = Popen(['./text.py'], stdin=PIPE, stdout=STDOUT, stderr=PIPE)
    Thread(target=print_output, args=(p.stdout,)).start()
    p.stdin.write("TESTE")
    p.stdin.close()

