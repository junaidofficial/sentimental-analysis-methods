ifeel_project README
==================

install
=======
python setup.py

check everything working
========================
python test.py

run
===
Read test.py

Please cite us:
===============

@inproceedings{araujo2014ifeel,
  title={ifeel: A system that compares and combines sentiment analysis methods},
  author={Ara{\'u}jo, Matheus and Gon{\c{c}}alves, Pollyanna and Cha, Meeyoung and Benevenuto, Fabr{\'\i}cio},
  booktitle={Proceedings of the companion publication of the 23rd international conference on World wide web companion},
  pages={75--78},
  year={2014},
  organization={International World Wide Web Conferences Steering Committee}
}


Sorry to be brief,  send an email to matheus.araujo@dcc.ufmg.br if any questions.

