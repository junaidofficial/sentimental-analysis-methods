#!/usr/bin/python

from subprocess import *
from optparse import OptionParser
import sys
import os
import string
from glob import glob

#Send python to script dir
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
os.chdir(SCRIPT_DIR)

import re

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

if __name__ == '__main__':
	parser = OptionParser()
	parser.add_option('-f', '--filename', dest='filename')
	parser.add_option('-t', '--text', dest='text')
	options, args = parser.parse_args()

	if len(sys.argv) != 3:
		print "Remeber the parameters: [-t|-f] ['text'|input_file]"
		sys.exit(0)

	elif options.text:
		text = options.text
		text = text.replace(",","").replace(";","").replace("\n","").replace(".","").replace("!","").replace("?","").replace(".","")
		text = text + " .\n"
		out = check_output("echo '" + text + "' | java -cp '*' -mx5g edu.stanford.nlp.sentiment.SentimentPipeline -stdin 2> /dev/null",shell=True)
		if "Negative" in out:
			print -1

		if "Positive" in out:
			print 1

		if "Neutral" in out:
			print 0

	elif options.filename:
		from uuid import uuid4 as uuid
		base_name = "." + str(uuid()).replace("-","")
		out_dir_name = SCRIPT_DIR + "/" + base_name
		os.makedirs(out_dir_name)
		#Fix input files, remove points, put points in the end
		tmpfile_filename_base = out_dir_name + "/.tmp_"
		output_filename = out_dir_name + "/.out"
		
		line_count = 0
		file_count = 0
		with open(options.filename,"r") as f:
			for line in f:
				if line_count % 100 == 0:
					tmpfile = open(tmpfile_filename_base + str(file_count),"w")
					file_count += 1
				line = line.lstrip() #Remove initial and ending whitespace
				#Remove all non ascii
				line = ''.join([i if ord(i) < 128 else ' ' for i in line])
				for char in string.punctuation:
					line = line.replace(char," ")
				line = line.strip() #Remove initial and ending whitespace
				text = line + " . "
				tmpfile.write(text)
				line_count += 1
				if line_count % 100 == 0:
					tmpfile.close()
			f.close()
		tmpfile.close()
		input_files = glob(out_dir_name + "/.tmp_*")
		input_files = natural_sort(input_files)
		for infile in input_files:
			call("java -cp '*' -mx5g edu.stanford.nlp.sentiment.SentimentPipeline -file " + infile + " >> " + output_filename + " 2> /dev/null   ",shell=True)
		TEXT_LINE = False
		with open(output_filename,"r") as fileout:
			for line in fileout:
				line = line.lower()
				TEXT_LINE = not TEXT_LINE
				
				if TEXT_LINE:
					pass

				if TEXT_LINE % 2 == 0:
					if "negative" in line:
						print -1

					if "positive" in line:
						print 1

					if "neutral" in line:
						print 0

		import shutil
		shutil.rmtree(out_dir_name)

