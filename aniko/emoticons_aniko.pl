 #!/usr/bin/perl -w
use warnings;
use Time::Local;
use HTML::Filter;
use HTML::Entities;
use Lingua::StopWords qw( getStopWords );
use List::Util qw/max/;

my $arg1; #-t ou -f
my $arg2; #tweet ou arquivo
my $c = 0;
our $text;

foreach my $argnum (0 .. $#ARGV)
{
    if($c == 0)
    {
         $arg1 = $ARGV[$argnum];
         $c+=1;
    }
    elsif($c == 1)
    {
        $arg2 = $ARGV[$argnum];
        $c+=1;
    }
}

our $stopwords = getStopWords('en');

our $dic = "emoticon.words.advanced";

open(IN, "$dic") or die "could not open $dic\n$!\n";

our %hash = ();
while (my $line = <IN>)
{
    chomp $line;
    our @linha_tokenizada = split('\t', $line);
    
    if(not exists $hash{$linha_tokenizada[2]})
    {
        if($linha_tokenizada[0] > $linha_tokenizada[1])
        {
            $hash{$linha_tokenizada[2]} = 1;
        }
        elsif($linha_tokenizada[0] < $linha_tokenizada[1])
        {
            $hash{$linha_tokenizada[2]} = -1;
        }
        elsif($linha_tokenizada[0] = $linha_tokenizada[1])
        {
            $hash{$linha_tokenizada[2]} = 0;
        }       
    }   
}

if($arg1 eq "-t")
{
    $text = $arg2;
    &trataTweet;
 }
elsif($arg1 eq "-f")
{
    &abreArqs;
}

sub trataTweet
{
	 $texthtml = decode_entities($text); #replaces HTML entities found in the $string with the corresponding ISO-8859/1. (Unrecognized entities are left alone)

    my @words_v = split(" ", $texthtml); #separa palavras do tweet
    my @words_v2 = grep { !$stopwords->{$_} } @words_v; #remove stopwords

    my $pos = 0;
    my $neg = 0;
    my $neut = 0;

    foreach(@words_v2)
        {
        my $word = lc $_;

        if(exists $hash{$word})
        {
            if($hash{$word} == 1)
            {
                $pos = $pos + 1;
            }
            elsif($hash{$word} == -1)
            {
                $neg = $neg + 1;
            }
            elsif($hash{$word} == 0)
            {
                $neut = $neut + 1;
            }
        }
    }

    if($pos > $neg and $pos > $neut)
    {
        print "1\n";
    }
    elsif($neg > $pos and $neg > $neut)
    {
        print "-1\n";
    }
     else
    {
        print  "0\n";
    }

}

sub abreArqs
{
 open(IN2, "$arg2") or die "could not open $arg2\n";

while (my $line = <IN2>)
{
    chomp $line;
    
    $texthtml = decode_entities($line);	#replaces HTML entities found in the $string with the corresponding ISO-8859/1. (Unrecognized entities are left alone)
    
    my @words_v = split(" ", $texthtml); #separa palavras do tweet
    my @words_v2 = grep { !$stopwords->{$_} } @words_v; #remove stopwords
    
    my $pos = 0;
    my $neg = 0;
    my $neut = 0;
    
    foreach(@words_v2)
	{
        my $word = lc $_;
        
        if(exists $hash{$word})
        {
            if($hash{$word} == 1)
            {
                $pos = $pos + 1;
            }
            elsif($hash{$word} == -1)
            {
                $neg = $neg + 1;
            }
            elsif($hash{$word} == 0)
            {
                $neut = $neut + 1;
            }           
        }
    }
    
    if($pos > $neg and $pos > $neut)
    {
        print "1\n";
    }
    elsif($neg > $pos and $neg > $neut)
    {
        print "-1\n";
    }
    else
    {
        print  "0\n";
    }
}
}

close IN;
close IN2;


